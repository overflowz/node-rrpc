const lib = require('./lib');

module.exports = {
  RRPCServer: lib.RRPCServer,
  RRPCClient: lib.RRPCClient,
};
