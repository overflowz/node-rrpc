const crypto = require('crypto');

function sign(message, privKey) {
  const signer = crypto.createSign('sha256');
  signer.end(Buffer.from(message));

  return signer.sign(privKey, 'hex');
}

function verify(message, pubKey, signature) {
  const verifier = crypto.createVerify('sha256');
  verifier.end(Buffer.from(message));

  return verifier.verify(pubKey, signature, 'hex');
}

module.exports = {
  sign,
  verify,
};
