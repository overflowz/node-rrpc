const _ = require('lodash');
const serializeError = require('serialize-error');

const typeToCode = {
  date: 1,
  error: 2,
};

const codeToType = {
  1: 'date',
  2: 'error',
};

function dateToRpcObj(date) {
  return { __type__: typeToCode.date, v: Date.parse(date) };
}

function rpcObjToDate(obj) {
  return new Date(obj.v);
}

function errorToRpcObj(error) {
  return { __type__: typeToCode.error, v: serializeError(error) };
}

function rpcObjToError(obj) {
  const err = new Error();

  Object.keys(obj.v).forEach(prop => {
    err[prop] = obj.v[prop];
  });

  return err;
}

function serializeCustomizer(value) {
  if (_.isDate(value)) {
    return dateToRpcObj(value);
  } else if (_.isError(value)) {
    return errorToRpcObj(value);
  }
}

function deserializeCustomizer(value) {
  if (value && _.isObject(value) && value.__type__ === typeToCode.date) {
    return rpcObjToDate(value);
  } else if (value && _.isObject(value) && value.__type__ === typeToCode.error) {
    return rpcObjToError(value);
  }
}

function serialize(val) {
  const _val = val;

  if (!_.isDate(val) && !_.isError(val) && (_.isObject(val) || _.isArray(val))) {
    val = _.cloneDeepWith(val, serializeCustomizer);
  } else {
    val = val && serializeCustomizer(val);
  }

  return val ? val : _val;
}

function deserialize(val) {
  return val && _.cloneDeepWith(val, deserializeCustomizer);
}

module.exports = {
  serialize,
  deserialize,
};
