const { RRPCServer, RRPCClient } = require('../lib');

const privateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJBAJ0YIfW0z9f6hyIYz8ohq3YD8FArrG3wsqm+TOOnVucBBgGSQcLS
xcZ7DsmvsYhxl3Rg1oqKaP2sY8kSLKBc2HECAwEAAQJADrtuo947B6Qk/BubkPcr
XzvrU3llCEJkXurLxyWwTqVyCNzN4LXjs1IhppyYxrd54v5MI/U9FoVPAQYaSkwC
gQIhAOJjNKjzYbP+CsJ60LfD6ZO7DBtLzGpsC5B1escAknZpAiEAsaSUIbPghByr
EmEa0KSdUNiQKjLFnXGfQzFjxpNy4MkCIAKNgdOB4xEnhTGKNv5LfD/JNSq9oRF2
JG89k+PYks45AiEAsWFFCOI0Lbbym0ebHvtAmpmkWn6YFL5/6PhISOCytQECIFR2
/1hu701D4TYvGDddmPR0MrFu/D8JKhfUicIMhm8Y
-----END RSA PRIVATE KEY-----`;

const publicKey = `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJ0YIfW0z9f6hyIYz8ohq3YD8FArrG3w
sqm+TOOnVucBBgGSQcLSxcZ7DsmvsYhxl3Rg1oqKaP2sY8kSLKBc2HECAwEAAQ==
-----END PUBLIC KEY-----`;

const rpcServer = new RRPCServer({ secure: true, key: publicKey, channel: 'my_rpc_channel' });
const rpcClient = new RRPCClient({ secure: true, key: privateKey, channel: 'my_rpc_channel' });

// callee setup

// register handlers like this
rpcServer.handlers = {
  // normal function
  normalFunctionWithArguments: (x, y) => {
    return x + y;
  },
  callbackFunction: (name, cb) => {
    return cb(null, 'hello ' + name);
  },
  // promise based function
  promiseFunction: () => {
    return new Promise((resolve, reject) => {
      setTimeout(resolve.bind(null, 'hello!'), 5000);
    });
  },
};

// or this
rpcServer.attachHandler('attachedFunc', (x, y) => x * y);

rpcServer.connect().then(() => {
  console.log('rpc server ready');
});

// caller side

rpcClient.connect().then(() => {
  console.log('connected to rpc channel');

  // first argument is event name.
  const normalFunctionWithArguments = rpcClient.createInvoker('normalFunctionWithArguments');
  const callbackFunction = rpcClient.createInvoker('callbackFunction', { isCallback: true, timeoutMS: 1000 });
  const promiseFunction = rpcClient.createInvoker('promiseFunction', { timeoutMS: 6000 });
  const attachedFunc = rpcClient.createInvoker('attachedFunc');

  // supports only promises for now, sorry!
  normalFunctionWithArguments(1, 2).then(console.log); // -> 3
  callbackFunction('RPC').then(console.log); // -> hello RPC
  promiseFunction().then(console.log); // prints 'hello!' after 5 seconds.

  // we can also pass callback in the end if we don't want to use promises.
  // WARNING: do not pass last argument of function type or promises will not work.
  attachedFunc(3, 4, (err, resp) => {
    console.log(resp); // -> 12
  });
});
